-- MySQL dump 10.16  Distrib 10.1.26-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: db
-- ------------------------------------------------------
-- Server version	10.1.26-MariaDB-0+deb9u1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `dbo.Contacts`
--

DROP TABLE IF EXISTS `dbo.Contacts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dbo.Contacts` (
  `name` varchar(4) DEFAULT NULL,
  `telephone` bigint(20) DEFAULT NULL,
  `address` varchar(7) DEFAULT NULL,
  `belongsTo` varchar(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dbo.Contacts`
--

LOCK TABLES `dbo.Contacts` WRITE;
/*!40000 ALTER TABLE `dbo.Contacts` DISABLE KEYS */;
INSERT INTO `dbo.Contacts` VALUES ('bob1',4501234564,'124 abc','Alice'),('bob2',4501234563,'123 abc','Alice'),('bob3',4501234562,'122 abc','Alice'),('bob4',4501234561,'121 abc','Alice');
/*!40000 ALTER TABLE `dbo.Contacts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dbo.Roles`
--

DROP TABLE IF EXISTS `dbo.Roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dbo.Roles` (
  `role` varchar(13) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dbo.Roles`
--

LOCK TABLES `dbo.Roles` WRITE;
/*!40000 ALTER TABLE `dbo.Roles` DISABLE KEYS */;
INSERT INTO `dbo.Roles` VALUES ('Administrator'),('Normal User');
/*!40000 ALTER TABLE `dbo.Roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dbo.Users`
--

DROP TABLE IF EXISTS `dbo.Users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dbo.Users` (
  `userName` varchar(13) DEFAULT NULL,
  `password` varchar(9) DEFAULT NULL,
  `role` varchar(13) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dbo.Users`
--

LOCK TABLES `dbo.Users` WRITE;
/*!40000 ALTER TABLE `dbo.Users` DISABLE KEYS */;
INSERT INTO `dbo.Users` VALUES ('Administrator','abc123...','Administrator'),('Alice','abc123...','Administrator'),('Bob','abc123...','Normal User');
/*!40000 ALTER TABLE `dbo.Users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-08-22 15:20:24
