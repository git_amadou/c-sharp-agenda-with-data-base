USE MASTER

IF EXISTS(SELECT * FROM master.sys.databases WHERE name = 'TPCSharp')
BEGIN
	DROP DATABASE TPCSharp
END

CREATE DATABASE TPCSharp

GO

USE TPCSharp

CREATE TABLE Roles(
	role varchar(30) NOT NULL PRIMARY KEY CLUSTERED,
	CONSTRAINT constraint_role CHECK (role LIKE 'Administrator' OR role LIKE 'Normal User')
)

GO

CREATE TABLE Users (
    userName varchar(30) NOT NULL PRIMARY KEY CLUSTERED,
	password varchar(30) NOT NULL,
	role varchar(30) NOT NULL FOREIGN KEY REFERENCES Roles(role)
)

GO

CREATE TABLE Contacts (
	name varchar(30) NOT NULL,
	telephone varchar(10) NULL,
	address varchar(30) NULL,
	belongsTo varchar(30) NOT NULL FOREIGN KEY REFERENCES Users(userName)
	CONSTRAINT PK_name_belongsTo PRIMARY KEY CLUSTERED (name, belongsTo)
)

GO


INSERT INTO Roles(role) VALUES('Administrator')
INSERT INTO Roles(role) VALUES('Normal User')

INSERT INTO Users(userName, password, role) VALUES('Administrator', 'abc123...', 'Administrator')
INSERT INTO Users(userName, password, role) VALUES('Alice', 'abc123...', 'Administrator')
INSERT INTO Users(userName, password, role) VALUES('Bob', 'abc123...', 'Normal User')

INSERT INTO Contacts(name, address, telephone, belongsTo) VALUES('bob1', '124 abc', '4501234564', 'Alice')
INSERT INTO Contacts(name, address, telephone, belongsTo) VALUES('bob2', '123 abc', '4501234563', 'Alice')
INSERT INTO Contacts(name, address, telephone, belongsTo) VALUES('bob3', '122 abc', '4501234562', 'Alice')
INSERT INTO Contacts(name, address, telephone, belongsTo) VALUES('bob4', '121 abc', '4501234561', 'Alice')


--SELECT *
--FROM Contacts

--SELECT *
--FROM Users

--SELECT *
--FROM Roles
