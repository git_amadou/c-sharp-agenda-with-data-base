﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


public class User
{
    public string UserName { get; set; }
    public string Password { get; set; }
    public string Role { get; set; }
}

public class Contact
{
    public string Name { get; set; }
    public string Telephone { get; set; }
    public string Address { get; set; }

    public override string ToString()
    {
        return this.Name + " " + " " + this.Telephone + " " + this.Address;
    }
}


