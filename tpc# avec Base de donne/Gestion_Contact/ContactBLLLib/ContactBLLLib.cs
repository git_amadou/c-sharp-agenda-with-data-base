﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


public class ContactManager
{
    static User user;

    //Partie user

    public static bool CreateUser(User user)
    {
        return DatabaseAccess.InsertUser(user);
    }

    public static bool ModifyPassword(string username, string password)
    {
        return DatabaseAccess.ModifyPassword(username, password);
    }

    public static User Login(string username, string password)
    {
        return ContactManager.user = DatabaseAccess.Login(username, password);
    }

    public static bool DeleteUser(string username)
    {
        return DatabaseAccess.DeleteUser(username);
    }

    public static void Logout()
    {
        ContactManager.user = null;
    }

    public static List<User> Getusers()
    {
        return DatabaseAccess.GetUsers();
    }

    //Partie contact

    public static bool AddContact(string name, string telephone, string address)
    {
        return DatabaseAccess.InsertContact(ContactManager.user,name, address, telephone);
    }

    public static bool DeleteContact(string name)
    {
        return DatabaseAccess.DeleteContact(ContactManager.user, name);
    }

    public static Contact FindContact(string name)
    { 
        return DatabaseAccess.FindContact(ContactManager.user, name);
    }

    public static bool ModifyContact(string name, string newName, string newTelephone, string newAddress)
    {
        return DatabaseAccess.ModifyContact(ContactManager.user, name, newName, newAddress, newTelephone);
    }

    public static List<Contact> GetContacts()
    {
        return DatabaseAccess.GetContacts(ContactManager.user);
    }

    public static List<Contact> GetContactsByCriteria(string criteria, string value)
    {
        return DatabaseAccess.GetContactsByCriteria(ContactManager.user, criteria, value);
    }

    public static List<Contact> GetContactsOrderBy(string orderBy)
    {
        return DatabaseAccess.GetContactsOrderBy(ContactManager.user, orderBy);
    }

    public static List<Contact> GetContactsByCriteriaOrderBy(string criteria, string value, string orderBy)
    {
        return DatabaseAccess.GetContactsByCriteriaOrderBy(ContactManager.user, criteria, value, orderBy);
    }
}

