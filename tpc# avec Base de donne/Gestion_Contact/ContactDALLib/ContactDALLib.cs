﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public class  DatabaseAccess
{
    static string connStr = @"Data Source=W7-2016-SEC-70\SQLEXPRESS;Initial Catalog=TPCSharp;Integrated Security=True;Connect Timeout=30";

    //Partie User

    //Verifier que le user entre bien son username et password
    public static User Login(string username, string password)
    {
        User user = new User();
        using (SqlConnection conn = new SqlConnection(DatabaseAccess.connStr))
        {
             conn.Open();

            using (SqlCommand cmd = conn.CreateCommand())
            {
                cmd.CommandText = "SELECT * FROM users WHERE userName = '" + username + "' AND password = '" + password + "'";
           
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        user.UserName = reader.GetString(0);
                        user.Password = reader.GetString(1);
                        user.Role = reader.GetString(2);
                    }
                }
            }
        }

        return user;
    }

    //Inserer un user dans la table users
    public static bool InsertUser(User user)
    {
        bool aFonctionner = true;
        using (SqlConnection conn = new SqlConnection(DatabaseAccess.connStr))
        {
            conn.Open();
            using (SqlCommand cmd = conn.CreateCommand())
            {
                cmd.CommandText = "Insert Into Users(userName, password, role) Values(@userName, @password, @role)";

                cmd.Parameters.AddWithValue("@userName", user.UserName);
                cmd.Parameters.AddWithValue("@password", user.Password);
                cmd.Parameters.AddWithValue("@Role", user.Role);

                //Exception principale est que le user existe deja dans la base de donnee
                try
                {
                    cmd.ExecuteNonQuery();
                }
                catch(SqlException ex)
                {
                    aFonctionner = false;
                }
               
            }
        }

        return aFonctionner;
    }

    //Modifier le password d'un user dans la table users
    public static bool ModifyPassword(string username, string password)
    {
        bool aFonctionner = true;
        if (!string.IsNullOrEmpty(password))
        {
            using (SqlConnection conn = new SqlConnection(DatabaseAccess.connStr))
            {
                conn.Open();
                 
                using (SqlCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = @"select username from users where userName = '" + username + "'";
                    string name = "";

                    //On regarde si il y a un user avec ce username
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            name = reader.GetString(0);
                        }
                    }

                    if (name != string.Empty)
                    {
                        cmd.CommandText = "update Users set password = '" + password + "' where userName = '" + username + "'";
                        cmd.ExecuteNonQuery();
                    }
                    else
                    {
                        aFonctionner = false;
                    }

                }
            }
        }
        else
        {
            aFonctionner = false;
        }
       
        return aFonctionner;
    }

    //Delete un user dans la table users, il faut aussi deleter les contacts de ce user en premier
    public static bool DeleteUser(string username)
    {
        bool aFonctionner = true;

        using (SqlConnection conn = new SqlConnection(DatabaseAccess.connStr))
        {
            conn.Open();

            using (SqlCommand cmd = conn.CreateCommand())
            {
                string name = "";
                string pass = "";

                cmd.CommandText = "select * from users where username = '" + username + "'";

                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        name = reader.GetString(0);
                        pass = reader.GetString(1);
                    }
                }

                if (name != string.Empty)
                {
                    cmd.CommandText = "delete from contacts where belongsTo = '" + username + "'";
                    cmd.ExecuteNonQuery();
                    cmd.CommandText = "delete from users where username = '" + username + "'";
                    cmd.ExecuteNonQuery();
                }
                else
                {
                    aFonctionner = false;
                }
            }
        }

        return aFonctionner;
    }

    //Recuperer tous les usernames
    public static List<User> GetUsers()
    {
        List<User> contactList = new List<User>();

        using (SqlConnection conn = new SqlConnection(DatabaseAccess.connStr))
        {
            conn.Open();

            using (SqlCommand cmd = conn.CreateCommand())
            {
                cmd.CommandText = "SELECT userName FROM Users";
                

                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        User user = new User();

                        user.UserName = reader.GetString(0);

                        contactList.Add(user);
                    }
                }
            }
        }

        return contactList;
    }

    //Partie Contact

    //Inserer un contact dans la table Contacts
    public static bool InsertContact(User userPresent, string name, string address, string telephone)
    {
        bool aFonctionner = true;

        using (SqlConnection conn = new SqlConnection(DatabaseAccess.connStr))
        {
            conn.Open();

            using (SqlCommand cmd = conn.CreateCommand())
            {
                cmd.CommandText = "INSERT INTO contacts(name, telephone, address, belongsTo) VALUES(@name, @telephone, @address, @belongsTo)";
                cmd.Parameters.AddWithValue("@name", name);
                cmd.Parameters.AddWithValue("@telephone", telephone);
                cmd.Parameters.AddWithValue("@address", address);
                cmd.Parameters.AddWithValue("@belongsTo", userPresent.UserName);

                try
                {
                    cmd.ExecuteNonQuery();
                }
                catch (SqlException ex)
                {
                    aFonctionner = false;
                }
            }
        }

        return aFonctionner;
    }

    //Deleter un contact dans la table Contacts
    public static bool DeleteContact(User userPresent, string name)
    {
        bool aFonctionner = true;

        using (SqlConnection conn = new SqlConnection(DatabaseAccess.connStr))
        {
            conn.Open();

            using(SqlCommand cmd = conn.CreateCommand())
            {
                cmd.CommandText = "select * from contacts where belongsTo = '" + userPresent.UserName + "' AND name = '" + name + "'";

                cmd.ExecuteNonQuery();

                Contact contact = new Contact();
                string username = "";

                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        contact.Name = reader.GetString(0);
                        contact.Telephone = reader.GetString(1);
                        contact.Address = reader.GetString(2);
                        username = reader.GetString(3);
                    }
                }

                if (!string.IsNullOrEmpty(contact.Name))
                {
                    cmd.CommandText = "delete from contacts where belongsTo = '" + userPresent.UserName + "' AND name = '" + name + "'";
                    cmd.ExecuteNonQuery();
                }
                else
                {
                    aFonctionner = false;
                }
            }
        }
        return aFonctionner;
    }

    //Trouver contact
    public static Contact FindContact(User userPresent, string name)
    {
        Contact contact = new Contact();
        using (SqlConnection conn = new SqlConnection(DatabaseAccess.connStr))
        {
            conn.Open();

            using (SqlCommand cmd = conn.CreateCommand())
            {
                cmd.CommandText = "SELECT * FROM contacts WHERE belongsTo = '" + userPresent.UserName + "' AND name = '" + name +"'";
                cmd.ExecuteNonQuery();

                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        contact.Name = reader.GetString(0);
                        contact.Telephone = reader.GetString(1);
                        contact.Address = reader.GetString(2);
                    }
                } 
            }
        }

        return contact;
    }

    //Modifier contact
    public static bool ModifyContact(User userPresent, string name, string newName, string newAddress, string newTelephone)
    {
        bool aFonctionner = true;
        using (SqlConnection conn = new SqlConnection(DatabaseAccess.connStr))
        {
            conn.Open();

            using (SqlCommand cmd = conn.CreateCommand())
            {
                try
                {
                    cmd.CommandText = "update contacts set name = @newName, telephone = @newTelephone, address = @newAddress WHERE name=@name AND belongsTo = @belongUserName";
                    cmd.Parameters.AddWithValue("@newName", newName);
                    cmd.Parameters.AddWithValue("@newTelephone", newTelephone);
                    cmd.Parameters.AddWithValue("@newAddress", newAddress);
                    cmd.Parameters.AddWithValue("@name", name);
                    cmd.Parameters.AddWithValue("@belongUserName", userPresent.UserName);

                    cmd.ExecuteNonQuery();
                }
                catch (SqlException ex)
                {
                    aFonctionner = false;
                }
            }

            return aFonctionner;
        }
    }

    //Recuperer une liste de Contacts selon le user
    public static List<Contact> GetContacts(User userPresent)
    {
        List<Contact> contactList = new List<Contact>();

        using (SqlConnection conn = new SqlConnection(DatabaseAccess.connStr))
        {
            conn.Open();

            using (SqlCommand cmd = conn.CreateCommand())
            {
                cmd.CommandText = "SELECT name, telephone, address FROM contacts WHERE belongsTo=@username";
                cmd.Parameters.AddWithValue("@username", userPresent.UserName);

                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        Contact contact = new Contact();

                        contact.Name = reader.GetString(0);
                        contact.Telephone = reader.GetString(1);
                        contact.Address = reader.GetString(2);

                        contactList.Add(contact);
                    }
                }
            }
        }

        return contactList;
    }

    //Recuperer une liste de Contacts selon le user et un critere
    public static List<Contact> GetContactsByCriteria(User userPresent, string criteria, string value)
    {
        List<Contact> contactList = new List<Contact>();

        using (SqlConnection conn = new SqlConnection(DatabaseAccess.connStr))
        {
            conn.Open();

            using (SqlCommand cmd = conn.CreateCommand())
            {
                cmd.CommandText = "SELECT name, telephone, address FROM contacts WHERE belongsTo=@username AND " + criteria + " = '" + value + "'";
                cmd.Parameters.AddWithValue("@username", userPresent.UserName);
                cmd.Parameters.AddWithValue("@criteria", criteria);
                cmd.Parameters.AddWithValue("@value", value);

                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        Contact contact = new Contact();

                        contact.Name = reader.GetString(0);
                        contact.Telephone = reader.GetString(1);
                        contact.Address = reader.GetString(2);

                        contactList.Add(contact);
                    }
                }
            }
        }

        return contactList;
    }

    //Recuperer une liste de contact selon un user et un ordre
    public static List<Contact> GetContactsOrderBy(User userPresent, string orderBy)
    {
        List<Contact> contactList = new List<Contact>();

        using (SqlConnection conn = new SqlConnection(DatabaseAccess.connStr))
        {
            conn.Open();

            using (SqlCommand cmd = conn.CreateCommand())
            {
                cmd.CommandText = "SELECT name, telephone, address FROM contacts WHERE belongsTo=@username ORDER BY " + orderBy;
                cmd.Parameters.AddWithValue("@username", userPresent.UserName);

                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        Contact contact = new Contact();

                        contact.Name = reader.GetString(0);
                        contact.Telephone = reader.GetString(1);
                        contact.Address = reader.GetString(2);

                        contactList.Add(contact);
                    }
                }
            }
        }

        return contactList;
    }

    //Recuperer une liste de Contacts selon le user, un critere et un ordre
    public static List<Contact> GetContactsByCriteriaOrderBy(User userPresent, string criteria, string value, string orderBy)
    {
        List<Contact> contactList = new List<Contact>();

        using (SqlConnection conn = new SqlConnection(DatabaseAccess.connStr))
        {
            conn.Open();

            using (SqlCommand cmd = conn.CreateCommand())
            {
                cmd.CommandText = "SELECT name, telephone, address FROM contacts WHERE belongsTo=@username AND " + criteria + " = '" + value + "'" + " ORDER BY " + orderBy;
                cmd.Parameters.AddWithValue("@username", userPresent.UserName);
                cmd.Parameters.AddWithValue("@criteria", criteria);
                cmd.Parameters.AddWithValue("@value", value);

                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        Contact contact = new Contact();

                        contact.Name = reader.GetString(0);
                        contact.Telephone = reader.GetString(1);
                        contact.Address = reader.GetString(2);

                        contactList.Add(contact);
                    }
                }
            }
        }

        return contactList;
    }

}

