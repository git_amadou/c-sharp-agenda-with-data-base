﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Gestion_Contact
{
    /// <summary>
    /// Interaction logic for DeleteUser.xaml
    /// </summary>
    public partial class DeleteUser : Window
    {

        ShowOptions showOptions;

        public DeleteUser(ShowOptions showOptions)
        {
            InitializeComponent();
            this.showOptions = showOptions;
            this.showOptions.Hide();
            this.Left = this.showOptions.Left;
            this.Top = this.showOptions.Top;
            this.Show();

            List<User> listeUser = ContactManager.Getusers();

            foreach (User user in listeUser)
            {
                this.usernameComboBox.Items.Add(user.UserName);
            }

            this.usernameComboBox.SelectedIndex = 0;
        }

        private void deleteUserButton_Click(object sender, RoutedEventArgs e)
        {
            string username = this.usernameComboBox.SelectedItem.ToString();

            bool aFonctionner = ContactManager.DeleteUser(username);

            if (aFonctionner)
            {
                this.statusLabel.Content = "Status: Success, user was deleted";
                this.usernameComboBox.Items.Remove(username);
                this.usernameComboBox.SelectedIndex = 0;
            }
            else
            {
                this.statusLabel.Content = "Status: Failure, user wasn't deleted";
            }
        }

        private void closeButton_Click(object sender, RoutedEventArgs e)
        {
            this.showOptions.Left = this.Left;
            this.showOptions.Top = this.Top;
            this.showOptions.Show();
            this.Close();
        }
    }
}
