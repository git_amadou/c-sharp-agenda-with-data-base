﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Gestion_Contact
{
    /// <summary>
    /// Interaction logic for AddUser.xaml
    /// </summary>
    public partial class AddUser : Window
    {
        LoginPage loginPage;
        public AddUser(LoginPage loginPage)
        {
            InitializeComponent();
            this.loginPage = loginPage;
            this.loginPage.Hide();
            this.Left = this.loginPage.Left;
            this.Top = this.loginPage.Top;
            this.Show();
        }

        private void addUserButton_Click(object sender, RoutedEventArgs e)
        {
            this.AddUserFonction();
        }

        private void closeButton_Click(object sender, RoutedEventArgs e)
        {
            this.loginPage.Left = this.Left;
            this.loginPage.Top = this.Top;
            this.loginPage.Show();
            this.Close();
        }

        private void OnKeyDownHandler(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                this.AddUserFonction();
            }
        }

        private void AddUserFonction()
        {
            string username = this.usernameTextBox.Text;
            string password = this.passwordBox.Password;

            User user = new User() { UserName = username, Password = password, Role = "Normal User" };

            bool aFonctionner = ContactManager.CreateUser(user);

            if (aFonctionner)
            {
                this.statusLabel.Content = "Status: Success, user added";
            }
            else
            {
                this.statusLabel.Content = "Status: Failure, user wasn't added";
            }
        }
    }
}
