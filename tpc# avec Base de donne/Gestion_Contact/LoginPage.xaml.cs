﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Gestion_Contact
{
    /// <summary>
    /// Interaction logic for LoginPage.xaml
    /// </summary>
    public partial class LoginPage : Window
    {
        MainWindow mainWindow;
        public LoginPage(MainWindow mainWindow)
        {
            InitializeComponent();
            this.mainWindow = mainWindow;
            this.Show();
        }

        private void loginButton_Click(object sender, RoutedEventArgs e)
        {
            this.Login();
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            AddUser addUser = new Gestion_Contact.AddUser(this);
        }

        private void closeButton_Click(object sender, RoutedEventArgs e)
        {
            this.mainWindow.Close();
        }

        private void OnKeyDownHandler(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                this.Login();
            }
        }

        private void Login()
        {
            string username = this.usernameTextBox.Text;
            string password = this.passwordBox.Password;

            User user = ContactManager.Login(username, password);

            if (!string.IsNullOrEmpty(user.UserName))
            {
                this.statusLabel.Content = "Login Success";
                ShowOptions showOptions = new ShowOptions(this.mainWindow, user, this.Left, this.Top);
                this.Close();
            }
            else
            {
                this.statusLabel.Content = "Login Failed";
            }
        }        
    }
}
