﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Gestion_Contact
{
    /// <summary>
    /// Interaction logic for AddContact.xaml
    /// </summary>
    public partial class AddContact : Window
    {
        ShowOptions showOptions;
        public AddContact(ShowOptions showOptions)
        {
            InitializeComponent();
            this.showOptions = showOptions;
            this.showOptions.Hide();
            this.Left = this.showOptions.Left;
            this.Top = this.showOptions.Top;
            this.Show();

        }

        private void addContactButton_Click(object sender, RoutedEventArgs e)
        {
            this.EnterContact();
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            this.showOptions.Left = this.Left;
            this.showOptions.Top = this.Top;
            this.showOptions.Show();
            this.Close();
        }

        private void OnKeyDownHandler(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                this.EnterContact();
            }
        }

        private void EnterContact()
        {
            string name = this.nameTextBox.Text;
            string telephone = this.telephoneTextBox.Text;
            string address = this.addressTextBox.Text;

            bool aFonctionner = ContactManager.AddContact(name, telephone, address);

            if (aFonctionner)
            {
                this.statusLabel.Content = "Status: Success, contact added";
            }
            else
            {
                this.statusLabel.Content = "Status: Failure, contact not added";
            }
        }        
    }
}
