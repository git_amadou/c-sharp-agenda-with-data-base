﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Gestion_Contact
{
    /// <summary>
    /// Interaction logic for ModifyUser.xaml
    /// </summary>
    public partial class ModifyUser : Window
    {
        ShowOptions showOptions;
        public ModifyUser(ShowOptions showOptions)
        {
            InitializeComponent();
            this.showOptions = showOptions;
            this.showOptions.Hide();
            this.Left = this.showOptions.Left;
            this.Top = this.showOptions.Top;
            this.Show();

            List<User> listUser = ContactManager.Getusers();


            foreach (User user in listUser)
            {
                this.usernameComboBox.Items.Add(user.UserName);
            }

            this.usernameComboBox.SelectedIndex = 0;
        }

        private void changepasswordButton_Click(object sender, RoutedEventArgs e)
        {
            this.ChangePassword();
        }

        private void closeButton_Click(object sender, RoutedEventArgs e)
        {
            this.showOptions.Left = this.Left;
            this.showOptions.Top = this.Top;
            this.showOptions.Show();
            this.Close();
        }

        private void OnKeyDownHandler(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                this.ChangePassword();
            }
        }

        private void ChangePassword()
        {
            string username = this.usernameComboBox.SelectedItem.ToString();
            string password = this.passwordBox.Password;

            this.statusLabel.Content = ContactManager.ModifyPassword(username, password);

            bool aFonctioner = ContactManager.ModifyPassword(username, password);

            if (aFonctioner)
            {
                this.statusLabel.Content = "Status: Success, password Changed";
            }
            else
            {
                this.statusLabel.Content = "Status: Failure, password didn't change";
            }
        }
    }
}
