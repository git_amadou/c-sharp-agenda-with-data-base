﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Gestion_Contact
{
    /// <summary>
    /// Interaction logic for FindContactsByCriteria.xaml
    /// </summary>
    public partial class FindContactsByCriteria : Window
    {
        ShowOptions showOptions;
        public FindContactsByCriteria(ShowOptions showOptions)
        {
            InitializeComponent();
            this.showOptions = showOptions;
            this.showOptions.Hide();
            this.Left = this.showOptions.Left;
            this.Top = this.showOptions.Top;
            this.Show();
            this.criteriaComboBox.Items.Add("name");
            this.criteriaComboBox.Items.Add("telephone");
            this.criteriaComboBox.Items.Add("address");
            this.criteriaComboBox.SelectedIndex = 0;
        }

        private void searchButton_Click(object sender, RoutedEventArgs e)
        {
            this.FindContact();
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            this.showOptions.Left = this.Left;
            this.showOptions.Top = this.Top;
            this.showOptions.Show();
            this.Close();
        }


        private void OnKeyDownHandler(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                this.FindContact();
            }
        }

        private void FindContact()
        {
            List<Contact> contactList = ContactManager.GetContactsByCriteria(this.criteriaComboBox.SelectedItem.ToString(), this.valueTextBox.Text);
            ShowContactsCriteria showContacts = new ShowContactsCriteria(contactList, this.criteriaComboBox.SelectedItem.ToString(), this.valueTextBox.Text, this.showOptions, this);
            this.Close();

        }
    }
}
