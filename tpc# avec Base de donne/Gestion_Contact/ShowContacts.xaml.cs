﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Gestion_Contact
{
    /// <summary>
    /// Interaction logic for ShowContacts.xaml
    /// </summary>
    public partial class ShowContacts : Window
    {
        ShowOptions showOptions;
        List<Contact> contactList;
        public ShowContacts(List<Contact> contactList, ShowOptions showOptions)
        {
            InitializeComponent();
            this.showOptions = showOptions;
            this.showOptions.Hide();
            this.Left = this.showOptions.Left;
            this.Top = this.showOptions.Top;
            this.Show();
            this.contactList = contactList;

            foreach (Contact contact in this.contactList)
            {
                this.contactListBox.Items.Add(contact);
            }
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            NewContactListOrderBy("name");
        }

        private void orderByTelephoneButton_Click(object sender, RoutedEventArgs e)
        {
            NewContactListOrderBy("telephone");
        }

        private void orderByAddressButton_Click(object sender, RoutedEventArgs e)
        {
            NewContactListOrderBy("address");
        }

        private void NewContactListOrderBy(string orderBy)
        {
            this.contactListBox.Items.Clear();
            List<Contact> newContactList = ContactManager.GetContactsOrderBy(orderBy);

            this.contactList = newContactList;

            foreach (Contact contact in this.contactList)
            {
                this.contactListBox.Items.Add(contact);
            }
        }

        private void closeButton_Click(object sender, RoutedEventArgs e)
        {
            this.showOptions.Left = this.Left;
            this.showOptions.Top = this.Top;
            this.showOptions.Show();
            this.Close();
        }
    }
}
