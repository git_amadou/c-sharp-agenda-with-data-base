﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Gestion_Contact
{
    /// <summary>
    /// Interaction logic for AddUserAdministrator.xaml
    /// </summary>
    public partial class AddUserAdministrator : Window
    {
        ShowOptions showOptions;
        public AddUserAdministrator(ShowOptions showOptions)
        {
            InitializeComponent();
            this.roleComboBox.Items.Add("Normal User");
            this.roleComboBox.Items.Add("Administrator");
            this.roleComboBox.SelectedIndex = 0;
            this.showOptions = showOptions;
            this.showOptions.Hide();
            this.Left = this.showOptions.Left;
            this.Top = this.showOptions.Top;
            this.Show();
        }

        private void addUserButton_Click(object sender, RoutedEventArgs e)
        {
            this.AddUserAdmin();
        }

        private void closeButton_Click(object sender, RoutedEventArgs e)
        {
            this.showOptions.Left = this.Left;
            this.showOptions.Top = this.Top;
            this.showOptions.Show();
            this.Close();
        }

        private void OnKeyDownHandler(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                this.AddUserAdmin();
            }
        }

        private void AddUserAdmin()
        {
            User user = new User();

            user.UserName = this.usernameTextBox.Text;
            user.Password = this.passwordBox.Password;
            user.Role = this.roleComboBox.SelectedItem.ToString();

            bool aFonctionner = ContactManager.CreateUser(user);

            if (aFonctionner)
            {
                this.statusLabel.Content = "Status: Success, user added";
            }
            else
            {
                this.statusLabel.Content = "Status: Failure, user not added";
            }
        }
    }
}
