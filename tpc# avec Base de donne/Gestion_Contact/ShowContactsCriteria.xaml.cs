﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Gestion_Contact
{
    /// <summary>
    /// Interaction logic for ShowContactsCriteria.xaml
    /// </summary>
    public partial class ShowContactsCriteria : Window
    {
        List<Contact> contactList;
        string criteria;
        string value;
        ShowOptions showOptions;
        FindContactsByCriteria findContactsByCriteria;

        public ShowContactsCriteria(List<Contact> contactList, string criteria, string value, ShowOptions showOptions, FindContactsByCriteria findContactsByCriteria)
        {
            InitializeComponent();
            this.showOptions = showOptions;
            this.findContactsByCriteria = findContactsByCriteria;
           
            this.Left = this.findContactsByCriteria.Left;
            this.Top = this.findContactsByCriteria.Top;
            this.Show();
            this.contactList = contactList;
            this.criteria = criteria;
            this.value = value;

            foreach (Contact contact in this.contactList)
            {
                this.contactListBox.Items.Add(contact);
            }
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            NewContactListOrderBy("name");
        }

        private void orderByTelephoneButton_Click(object sender, RoutedEventArgs e)
        {
            NewContactListOrderBy("telephone");
        }

        private void orderByAddressButton_Click(object sender, RoutedEventArgs e)
        {
            NewContactListOrderBy("address");
        }

        private void NewContactListOrderBy(string orderBy)
        {
            this.contactListBox.Items.Clear();
            List<Contact> newContactList = ContactManager.GetContactsByCriteriaOrderBy(criteria, value, orderBy);

            this.contactList = newContactList;

            foreach (Contact contact in this.contactList)
            {
                this.contactListBox.Items.Add(contact);
            }
        }

        private void closeButton_Click(object sender, RoutedEventArgs e)
        {
            this.showOptions.Left = this.Left;
            this.showOptions.Top = this.Top;
            this.showOptions.Show();
            this.Close();
        }
    }
}
