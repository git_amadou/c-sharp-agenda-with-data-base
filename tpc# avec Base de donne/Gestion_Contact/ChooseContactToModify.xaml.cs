﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Gestion_Contact
{
    /// <summary>
    /// Interaction logic for ChooseContactToModify.xaml
    /// </summary>
    public partial class ChooseContactToModify : Window
    {
        ShowOptions showOptions;
        public ChooseContactToModify(ShowOptions showOptions)
        {

            InitializeComponent();
            this.showOptions = showOptions;
            this.showOptions.Hide();
            this.Left = this.showOptions.Left;
            this.Top = this.showOptions.Top;
            this.Show();

            List<Contact> listeContact = ContactManager.GetContacts();

            foreach (Contact contact in listeContact)
            {
                this.contactComboBox.Items.Add(contact.Name);
            }

            this.contactComboBox.SelectedIndex = 0;
        }

        private void closeButton_Click(object sender, RoutedEventArgs e)
        {
            this.showOptions.Left = this.Left;
            this.showOptions.Top = this.Top;
            this.showOptions.Show();
            this.Close();
        }

        private void chooseContactButton_Click(object sender, RoutedEventArgs e)
        {
            string name = this.contactComboBox.SelectedItem.ToString();

            Contact contact = ContactManager.FindContact(name);

            if (!string.IsNullOrEmpty(contact.Name))
            {
                this.statusLabel.Content = "Status: Found";
                ModifyContact modifyContact = new ModifyContact(name, contact.Name, contact.Telephone, contact.Address, this.showOptions, this);
                this.Close();
            }
            else
            {
                this.statusLabel.Content = "Status: Not Found";
            }
        }
    }
}
