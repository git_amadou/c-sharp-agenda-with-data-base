﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Gestion_Contact
{
    /// <summary>
    /// Interaction logic for ModifyContact.xaml
    /// </summary>
    public partial class ModifyContact : Window
    {
        string nameToModify;
        ShowOptions showOptions;
        ChooseContactToModify chooseContactToModify;
       
        public ModifyContact(string nameToModify, string newName, string newTelephone, string newAddress, ShowOptions showOptions, ChooseContactToModify chooseContactToModify)
        {
            InitializeComponent();
            this.showOptions = showOptions;
            this.chooseContactToModify = chooseContactToModify;

            this.Left = this.chooseContactToModify.Left;
            this.Top = this.chooseContactToModify.Top;
            this.nameToModify = nameToModify;
            this.nameTextBox.Text = newName;
            this.telephoneTextBox.Text = newTelephone;
            this.addressTextBox.Text = newAddress;
            this.Show();
        }

        private void modifyContactButton_Click(object sender, RoutedEventArgs e)
        {
            this.ModifyContactFunction();
        }

        private void closeButton_Click(object sender, RoutedEventArgs e)
        {
            this.showOptions.Left = this.Left;
            this.showOptions.Top = this.Top;
            this.showOptions.Show();
            this.Close();
        }


        private void OnKeyDownHandler(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                this.ModifyContactFunction();
            }
        }

        private void ModifyContactFunction()
        {
            string newName = this.nameTextBox.Text;
            string newTelephone = this.telephoneTextBox.Text;
            string newAddress = this.addressTextBox.Text;

            bool aFonctionner = ContactManager.ModifyContact(this.nameToModify, newName, newTelephone, newAddress);

            if (aFonctionner)
            {
                this.statusLabel.Content = "Status: Success, contact modified";
            }
            else
            {
                this.statusLabel.Content = "Status: Failure, contact not modified";
            }
        }
    }
}
