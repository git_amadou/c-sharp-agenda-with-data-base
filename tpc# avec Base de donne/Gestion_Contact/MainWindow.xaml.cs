﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Gestion_Contact
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            this.Hide();
            InitializeComponent();
            //AddUser addUser = new Gestion_Contact.AddUser();
            //ModifyUser modify = new Gestion_Contact.ModifyUser();
            LoginPage loginPage = new Gestion_Contact.LoginPage(this);
            //DeleteUser deleteUser = new Gestion_Contact.DeleteUser();
            //AddContact addContact = new Gestion_Contact.AddContact();
            //DeleteContact deleteContact = new Gestion_Contact.DeleteContact();
            //FindContact findContact = new Gestion_Contact.FindContact();
        }

        //Pris de https://stackoverflow.com/questions/9992119/wpf-app-doesnt-shut-down-when-closing-main-window
        //Pour fermer le processus apres qu'on fait x(ferme) sur le main windows
        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            Application.Current.Shutdown();
        }
    }
}
