﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Gestion_Contact
{
    /// <summary>
    /// Interaction logic for FindContact.xaml
    /// </summary>
    public partial class FindContact : Window
    {
        public FindContact()
        {
            InitializeComponent();
            this.Show();
        }

        private void findContactButton_Click(object sender, RoutedEventArgs e)
        {
            string name = this.nameTextBox.Text;

            Contact contact= ContactManager.FindContact(name);

            if (!string.IsNullOrEmpty(contact.Name))
            {
                this.statusLabel.Content = "Status: Found";
                ModifyContact modifyContact = new ModifyContact(name, contact.Name, contact.Telephone, contact.Address);
                this.Close();
            }
            else
            {
                this.statusLabel.Content = "Status: Not Found";
            }
        }

        private void closeButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
