﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Gestion_Contact
{
    /// <summary>
    /// Interaction logic for ShowOptions.xaml
    /// </summary>
    public partial class ShowOptions : Window
    {
        MainWindow mainWindow;
        User user;
        public ShowOptions(MainWindow mainWindow, User user, double positionLeft, double positionTop)
        {
            InitializeComponent();
            this.mainWindow = mainWindow;
            this.user = user;
            this.Left = positionLeft;
            this.Top = positionTop;
            this.Show();

            if (user.Role == "Administrator")
            {
                this.changePasswordButton.Visibility = Visibility.Visible;
                this.createUserButton.Visibility = Visibility.Visible;
                this.deleteUserButton.Visibility = Visibility.Visible;
            }
            else
            {
                this.changePasswordButton.Visibility = Visibility.Hidden;
                this.createUserButton.Visibility = Visibility.Hidden;
                this.deleteUserButton.Visibility = Visibility.Hidden;
            }
        }

        private void showContactsButton_Click(object sender, RoutedEventArgs e)
        {
            List<Contact> contactList = ContactManager.GetContacts();

            ShowContacts showContacts = new ShowContacts(contactList, this);
        }

        private void addContactButton_Click(object sender, RoutedEventArgs e)
        {
            AddContact addContact = new Gestion_Contact.AddContact(this);
        }

        private void modifyContactButton_Click(object sender, RoutedEventArgs e)
        {
            ChooseContactToModify findContact = new Gestion_Contact.ChooseContactToModify(this);
        }

        private void searchByCriteriaButton_Click(object sender, RoutedEventArgs e)
        {
            FindContactsByCriteria findContact = new Gestion_Contact.FindContactsByCriteria(this);
        }

        private void closeButton_Click(object sender, RoutedEventArgs e)
        {
            this.mainWindow.Close();
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            DeleteContact deleteContact = new DeleteContact(this);
        }

        private void backToLoginButton_Click(object sender, RoutedEventArgs e)
        {
            this.user = null;
            ContactManager.Logout();
            LoginPage loginPage = new LoginPage(this.mainWindow);
            loginPage.Left = this.Left;
            loginPage.Top = this.Top;
            this.Close();
        }

        private void createUserButton_Click(object sender, RoutedEventArgs e)
        {
            AddUserAdministrator addUserAdministrator = new AddUserAdministrator(this);
        }

        private void deleteUserButton_Click(object sender, RoutedEventArgs e)
        {
            DeleteUser deleteUser = new DeleteUser(this);
        }

        private void changePasswordButton_Click(object sender, RoutedEventArgs e)
        {
            ModifyUser modifyUser = new ModifyUser(this);
        }
    }
}
